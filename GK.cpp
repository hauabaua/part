/******************************************************************
Grafika komputerowa, �rodowisko MS Windows - Rysowanie krzywymi Beziera
*****************************************************************/

#include <windows.h>
//#include <iostream.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctime>
#include "World.h"

World* world = new World();
int iloscKulek = 10;
float gravity = 0.2f;
float r = 50;
float displayScale = 0.2f;

//deklaracja funkcji obslugi okna
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//funkcja Main - dla Windows
int WINAPI WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow)
{
	srand(time(NULL));


	MSG meldunek;		  //innymi slowy "komunikat"
	WNDCLASS nasza_klasa; //klasa g��wnego okna aplikacji
	HWND okno;
	static char nazwa_klasy[] = "Podstawowa";

	//Definiujemy klase g��wnego okna aplikacji
	//Okreslamy tu wlasciwosci okna, szczegoly wygladu oraz
	//adres funkcji przetwarzajacej komunikaty
	nasza_klasa.style = CS_HREDRAW | CS_VREDRAW;
	nasza_klasa.lpfnWndProc = WndProc; //adres funkcji realizuj�cej przetwarzanie meldunk�w 
	nasza_klasa.cbClsExtra = 0;
	nasza_klasa.cbWndExtra = 0;
	nasza_klasa.hInstance = hInstance; //identyfikator procesu przekazany przez MS Windows podczas uruchamiania programu
	nasza_klasa.hIcon = 0;
	nasza_klasa.hCursor = LoadCursor(0, IDC_ARROW);
	nasza_klasa.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	nasza_klasa.lpszMenuName = "Menu";
	nasza_klasa.lpszClassName = nazwa_klasy;

	//teraz rejestrujemy klas� okna g��wnego
	RegisterClass(&nasza_klasa);

	/*tworzymy okno g��wne
	okno b�dzie mia�o zmienne rozmiary, listw� z tytu�em, menu systemowym
	i przyciskami do zwijania do ikony i rozwijania na ca�y ekran, po utworzeniu
	b�dzie widoczne na ekranie */
	okno = CreateWindow(nazwa_klasy, "Kulky", WS_OVERLAPPEDWINDOW | WS_VISIBLE,
		CW_USEDEFAULT, CW_USEDEFAULT, 700, 400, NULL, NULL, hInstance, NULL);

	/* wyb�r rozmiaru i usytuowania okna pozostawiamy systemowi MS Windows */
	ShowWindow(okno, nCmdShow);

	// PARTICLESY <> PARTICLESY <> PARTICLESY <> PARTICLESY <> PARTICLESY <> PARTICLESY <> PARTICLESY <> PARTICLESY <> PARTICLESY <> 

	RECT p;
	GetClientRect(okno, &p);

	world->gravitySpeedIncrease = gravity;
	world->widthX = (p.right - p.left) / displayScale;
	world->widthY = (p.bottom - p.top) / displayScale;

	float xx, yy, vX, vY;

	for (int i = 0; i < iloscKulek; i++)
	{
		int maxX = world->widthX - 2 * r - 200 / displayScale;
		int maxY = world->widthY - 2 * r;
		xx = (rand() % maxX) + r;
		yy = (rand() % maxY) + r;
		vX = rand() % 100 * 0.01 - 0.5 + 5;
		vY = rand() % 100 * 0.01 - 0.5;
		Particle *p = new Particle(r, xx, yy, vX, vY, world);
		world->particles.push_back(*p);
	}

	// PARTICLESY <> PARTICLESY <> PARTICLESY <> PARTICLESY <> PARTICLESY <> PARTICLESY <> PARTICLESY <> PARTICLESY <> PARTICLESY <> 

	//odswiezamy zawartosc okna
	UpdateWindow(okno);



	// G��WNA P�TLA PROGRAMU
	while (GetMessage(&meldunek, NULL, 0, 0))

		/* pobranie komunikatu z kolejki; funkcja GetMessage zwraca FALSE tylko dla
		komunikatu wm_Quit; dla wszystkich pozosta�ych komunikat�w zwraca warto�� TRUE */
	{

		TranslateMessage(&meldunek); // wst�pna obr�bka komunikatu
		DispatchMessage(&meldunek);  // przekazanie komunikatu w�a�ciwemu adresatowi (czyli funkcji obslugujacej odpowiednie okno)
	}
	return meldunek.wParam;
}

/********************************************************************
FUNKCJA OKNA realizujaca przetwarzanie meldunk�w kierowanych do okna aplikacji*/
LRESULT CALLBACK WndProc(HWND okno, UINT kod_meldunku, WPARAM wParam, LPARAM lParam)
{
	HMENU mPlik, mInfo, mGlowne;

	/* PONI�SZA INSTRUKCJA DEFINIUJE REAKCJE APLIKACJI NA POSZCZEG�LNE MELDUNKI */
	switch (kod_meldunku)
	{
	case WM_CREATE:  //meldunek wysy�any w momencie tworzenia okna
		mPlik = CreateMenu();
		AppendMenu(mPlik, MF_STRING, 100, "&Zapiszcz...");
		AppendMenu(mPlik, MF_SEPARATOR, 0, "");
		AppendMenu(mPlik, MF_STRING, 101, "&Koniec");
		mInfo = CreateMenu();
		AppendMenu(mInfo, MF_STRING, 200, "&Autor...");
		mGlowne = CreateMenu();
		AppendMenu(mGlowne, MF_POPUP, (UINT)mPlik, "&Plik");
		AppendMenu(mGlowne, MF_POPUP, (UINT)mInfo, "&Informacja");
		SetMenu(okno, mGlowne);
		DrawMenuBar(okno);

		SetTimer(okno, WM_TIMER, 20, NULL);

		return 0;

	case WM_COMMAND: //reakcje na wyb�r opcji z menu
		switch (wParam)
		{
		case 100: if (MessageBox(okno, "Zapiszcze�?", "Pisk", MB_YESNO) == IDYES)
			MessageBeep(0);
			break;
		case 101: DestroyWindow(okno); //wysylamy meldunek WM_DESTROY
			break;
		case 200: MessageBox(okno, "Imi� i nazwisko:\nNumer indeksu: ", "Autor", MB_OK);
		}
		return 0;
	case WM_TIMER:
	{
		if (world->time<=100)
		{
			int maxX = world->widthX - 2 * r - 600 / displayScale;
			int maxY = world->widthY - 2 * r - 100 / displayScale;
			float xx = (rand() % maxX) + r;
			float yy = (rand() % maxY) + r;
			float vX = rand() % 100 * 0.01 - 0.5 + 5;
			float vY = rand() % 100 * 0.01 - 0.5;
			Particle *p = new Particle(r, xx, yy, vX, vY, world);
			world->particles.push_back(*p);
		}

		for (int i = 0; i < 1; i++)
		{
			world->nextStep();
		}



		InvalidateRect(okno, NULL, true);
	}

	case WM_LBUTTONDOWN: //reakcja na lewy przycisk myszki
	{
		int x = LOWORD(lParam);
		int y = HIWORD(lParam);

		return 0;
	}

	case WM_LBUTTONUP: //reakcja na lewy przycisk myszki
	{
		int x = LOWORD(lParam);
		int y = HIWORD(lParam);

	}

	case WM_MOUSEMOVE:
	{
		int x = LOWORD(lParam);
		int y = HIWORD(lParam);

	}
	case WM_PAINT:
	{
		PAINTSTRUCT paint;
		HDC kontekst;

		kontekst = BeginPaint(okno, &paint);

		HPEN pioro = CreatePen(PS_SOLID, 6, RGB(255, 0, 0));
		SelectObject(kontekst, pioro);

		//int ret = PolyBezier(kontekst, punkty, liczba_pkt);

		HPEN 	pioro4 = CreatePen(PS_SOLID, 1, RGB(0, 0, 0));

		HBRUSH wyp2 = CreateSolidBrush(RGB(127, 127, 0)),
			wyp3 = CreateSolidBrush(RGB(0, 127, 127));



		SelectObject(kontekst, pioro4);

		for (int i = 0; i < world->particleCount; i++)
		{
			Ellipse(kontekst, world->particles[i].xx * displayScale - world->particles[i].r* displayScale, world->particles[i].yy* displayScale - world->particles[i].r* displayScale,
				world->particles[i].xx* displayScale + world->particles[i].r* displayScale, world->particles[i].yy* displayScale + world->particles[i].r* displayScale);
		}

		DeleteObject(pioro);
		DeleteObject(pioro4);
		DeleteObject(wyp2);
		DeleteObject(wyp3);

		EndPaint(okno, &paint);

		return 0;
	}

	case WM_DESTROY: //obowi�zkowa obs�uga meldunku o zamkni�ciu okna
		PostQuitMessage(0);
		return 0;

	default: //standardowa obs�uga pozosta�ych meldunk�w
		return DefWindowProc(okno, kod_meldunku, wParam, lParam);
	}
}
