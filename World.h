#pragma once
#include <iostream>
#include <vector>

#include <Windows.h>

using namespace std;

class Particle;

class World
{
public:
	float widthX;
	float widthY;
	float gravitySpeedIncrease;
	int time = 0;
	int particleCount = 0;
	//int worldState[1600];
	vector<Particle> particles;
	World(float _widthX = 800, float _widthY = 250);
	~World(void);
	
	void nextStep();
	void showWorld();
};

class Particle
{
public:
	World* world = nullptr;
	float r;	// tak naprawde srednica
	float xx;
	float yy;
	float Vx;	// docelowo class vector2d
	float Vy;
	float stiffness = 0.01;
	float wspOdb = 0.95;
	Particle();
	Particle(const Particle* par);
	Particle(float _r, float _xx, float _yy, float _Vx, float _Vy, World *world);
	~Particle(void);
	void collision(Particle* wasHit);
	void showParticle();
	void move();
};