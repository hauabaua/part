#include "World.h"

using namespace std;

World::World(float _widthX, float _widthY)
{
	gravitySpeedIncrease = 0.0f;
	time = 0;
	widthX = _widthX;
	widthY = _widthY;
}

World::~World(void)
{
}

void World::nextStep()
{
	//system("cls");

	Particle *par;
	for (int i = 0; i < particles.size(); i++)
	{
		par = &particles[i];
		par->move();
	}

	//showWorld();
	time++;
}

void World::showWorld()
{
	Particle *par;
	for (int i = 0; i < particles.size(); i++)
	{
		par = &particles[i];
		COORD coord;
		coord.X = par->xx / 10;
		coord.Y = par->yy / 10;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
		cout << 'o';
	}
}