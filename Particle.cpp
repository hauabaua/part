#include "World.h"
#include <cmath>


Particle::Particle()
{
	r = 0;
	xx = 0;
	yy = 0;
	Vx = 0;
	Vy = 0;
}

Particle::Particle(const Particle *par) {
	r = par->r;
	xx = par->xx;
	yy = par->yy;
	Vx = par->Vx;
	Vy = par->Vy;
}

Particle::Particle(float _r, float _xx, float _yy, float _Vx, float _Vy, World *_world)
{
	r = _r;
	xx = _xx;
	yy = _yy;
	Vx = _Vx;
	Vy = _Vy;
	world = _world;
	world->particleCount++;
}

Particle::~Particle(void)
{
}

void Particle::showParticle()
{

}

void Particle::move()
{
	// CHECK FOR COLLISION, NEED TO BE IMPROVED, NOW ITS O(n^2) PROGRAM
	for (int i = 0; i < world->particles.size(); i++)
	{
		if (this != &world->particles[i])
		{
			collision(&world->particles[i]);
		}
	}
	if ((xx + Vx <= r) || (xx + Vx >= world->widthX - r)) Vx = -Vx * wspOdb;
	if ((yy + Vy <= r) || (yy + Vy >= world->widthY - r)) Vy = -Vy * wspOdb;

	// If particle go too far it's moved a little
	float X = 0.9f;
	float T = 0.2f;	// additional translate factor
	(xx + Vx <= X*r) ? xx += T*r + Vx : xx += Vx;
	(xx + Vx >= world->widthX - X*r) ? xx += -T*r + Vx : xx += Vx;
	(yy + Vy <= X*r) ? yy += T*r + Vy : yy += Vy;
	(yy + Vy >= world->widthY - X*r) ? yy += -T*r + Vy : yy += Vy;



	// GRAVITY
	Vy += world->gravitySpeedIncrease;
}

void Particle::collision(Particle* wasHit) {
	float xx1 = wasHit->xx;
	float yy1 = wasHit->yy;

	// roznica predkosci
	float deltaVx = wasHit->Vx - Vx;
	float deltaVy = wasHit->Vy - Vy;

	// skladowe kierunku odbicia (roznice odleglosci)

	//float deltaX1 = xx1 - xx;
	//float deltaY1 = yy1 - yy;

	/*float deltaX = xx1 - xx - deltaVx;
	float deltaY = yy1 - yy - deltaVy;*/

	// roznica polozenia w chwili zderzenia
	float deltaX = xx1 - xx;
	float deltaY = yy1 - yy;

	//float distance1 = sqrt(deltaX1*deltaX1 + deltaY1*deltaY1);
	float distance = sqrt(deltaX*deltaX + deltaY*deltaY);
	float radiusSum = r + wasHit->r;

	// wsp uwzgledniajacy przenikanie sie czastek
	//float bounceFactor = riadiusSum / distance1 - 1;
	//float bounceFactor = riadiusSum - distance;


	// *** STIFF BOUNCE *** STIFF BOUNCE *** STIFF BOUNCE *** STIFF BOUNCE *** STIFF BOUNCE *** 

	//if (distance < radiusSum)
	//{
	//	// odbicie
	//	float goraUlamka = deltaVx*deltaX + deltaVy* deltaY * wspOdb;
	//	float deltaVu_x = goraUlamka * deltaX / (distance*distance);
	//	float deltaVu_y = goraUlamka * deltaY / (distance*distance);
	//	Vx += deltaVu_x;
	//	Vy += deltaVu_y;
	//	wasHit->Vx -= deltaVu_x;
	//	wasHit->Vy -= deltaVu_y;


	//	//float xxP = xx + Vx;
	//	//float yyP = yy + Vy;
	//	//float xx1P = wasHit->xx + wasHit->Vx;
	//	//float yy1P = wasHit->yy + wasHit->Vy;

	//	//float deltaXP = xx1P - xxP;
	//	//float deltaYP = yy1P - yyP;

	//	//float distance = sqrt(deltaXP*deltaXP + deltaYP*deltaYP);


	//	// separate particles if they are too close
	//	if (distance < 0.95*radiusSum)
	//	{
	//		float dx = deltaVu_x / goraUlamka*r;
	//		float dy = deltaVu_y / goraUlamka*r;

	//		xx -= dx;
	//		yy -= dy;
	//		wasHit->xx += dx;
	//		wasHit->yy += dy;

	//		//xx -= deltaVu_x / goraUlamka;
	//		//yy -= deltaVu_y / goraUlamka;
	//		//wasHit->xx += deltaVu_x / goraUlamka;
	//		//wasHit->yy += deltaVu_y / goraUlamka;

	//		////xx -= deltaVu_x / goraUlamka;
	//		////yy -= deltaVu_y / goraUlamka;
	//		////wasHit->xx += deltaVu_x / goraUlamka;
	//		////wasHit->yy += deltaVu_y / goraUlamka;
	//	}
	//}


	// *** REPULSION FORCE *** REPULSION FORCE *** REPULSION FORCE *** REPULSION FORCE *** REPULSION FORCE ***



	if (distance < 0.9*radiusSum)
	{
		// odbicie
		float goraUlamka = deltaVx*deltaX + deltaVy* deltaY * wspOdb;
		float deltaVu_x = goraUlamka * deltaX / (distance*distance);
		float deltaVu_y = goraUlamka * deltaY / (distance*distance);
		Vx += deltaVu_x;
		Vy += deltaVu_y;
		wasHit->Vx -= deltaVu_x;
		wasHit->Vy -= deltaVu_y;


		//float xxP = xx + Vx;
		//float yyP = yy + Vy;
		//float xx1P = wasHit->xx + wasHit->Vx;
		//float yy1P = wasHit->yy + wasHit->Vy;

		//float deltaXP = xx1P - xxP;
		//float deltaYP = yy1P - yyP;

		//float distance = sqrt(deltaXP*deltaXP + deltaYP*deltaYP);


		// separate particles if they are too close
		if (distance < 1.0*radiusSum)
		{
			float dx = deltaVu_x / goraUlamka*r;
			float dy = deltaVu_y / goraUlamka*r;

			xx -= dx;
			yy -= dy;
			wasHit->xx += dx;
			wasHit->yy += dy;

			//xx -= deltaVu_x / goraUlamka;
			//yy -= deltaVu_y / goraUlamka;
			//wasHit->xx += deltaVu_x / goraUlamka;
			//wasHit->yy += deltaVu_y / goraUlamka;

			////xx -= deltaVu_x / goraUlamka;
			////yy -= deltaVu_y / goraUlamka;
			////wasHit->xx += deltaVu_x / goraUlamka;
			////wasHit->yy += deltaVu_y / goraUlamka;
		}
	}

	float distance2 = distance - 1.6*r;
	if (distance > 0.9*radiusSum)
	{
		float repulseFactor = 0.3f * sqrt(r);
		//float repulseFactor = 2.0f;

		//float distance2 = distance - 1.7*r;

		float repulseForce = repulseFactor / (distance2*distance2*distance2);
		float dVx = repulseForce * deltaX;
		float dVy = repulseForce * deltaY;

		this->Vx -= dVx;
		this->Vy -= dVy;
		wasHit->Vx += dVx;
		wasHit->Vy += dVy;
	}
}