#include "World.h"
#pragma once

#include <iostream>

class Particle
{
public:
	World* world = nullptr;
	double r;
	double xx;
	double yy;
	double Vx;	// docelowo class vector2d
	double Vy;
	Particle();
	Particle(const Particle* par);
	Particle(double _r, double _xx, double _yy, double _Vx, double _Vy, World *world);
	~Particle(void);
	void collision(Particle* wasHit);
	void showParticle();
	void move();
};